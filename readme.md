In this task we are going to implement comment list with ability to create new one. 
Hours spent on this task will be compensated for candidate, that will be selected for job.

**Acceptance criteria**:

* Implement List of comments
* Implement Form for creating comments
* Use Design.png for reference(obviously it shouldn't be pixel perfect, but html should be clean)
* If comment more then 25 symbols then show error - 'Comment should be less then 25 characters'
* Send comment as POST request to "http://private-117005-reacttaskentry.apiary-mock.com/questions". User response to render new comment on UI.
* When finished create pull request or fork

**What matters**:

* Following Acceptance criteria
* Clean code
* Clean html/scss
* UI/UX quality
* Clean structure of files
* Code styling/Naming

**What's not that important**:

* Completeness of task. If task takes too much time - no need to implement everything, just provide detailed plan what would you do. 

**No need to implement**:

* Reply, like, view replies